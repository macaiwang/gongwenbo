from flask import Flask

# 创建flask对象
app = Flask(__name__)


@app.route('/')
def index():
    """返回的页面数据"""

    return 'heihei'


@app.route('/login.html')
def login():
    """处理登陆的逻辑"""

    return '<h1>登录页面</h1>'


# 运行
if __name__ == '__main__':
    app.run()
