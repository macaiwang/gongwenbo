from flask import Flask

# 创建flask对象
app = Flask(__name__)


@app.route('/')
def index():
    """返回的页面数据"""

    return 'heihei'


# 运行
if __name__ == '__main__':
    app.run()
